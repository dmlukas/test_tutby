package com.dml.test.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.activeandroid.query.Delete;
import com.dml.test.entity.RecordOpenScreen;

/**
 * Created by Dzmitry Lukashanets on 31.03.2015.
 */
public class AppIntentService extends IntentService {

    private static final String TAG = "AppIntentService";
    private static final String TAG_TIME = "time";

    public static final String ACTION_SAVE_OPEN_SCREEN = "saveOpenScreen";
    public static final String ACTION_CLEANING = "startCleaning";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public AppIntentService() {
        super(TAG);
    }

    public static void saveOpenScreen(Context context, long time) {
        context.startService(new Intent(context, AppIntentService.class).setAction(ACTION_SAVE_OPEN_SCREEN).putExtra(TAG_TIME, time));
    }

    public static void startCleaning(Context context) {
        context.startService(new Intent(context, AppIntentService.class).setAction(ACTION_CLEANING));
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.i(TAG, "onHandlerIntent = " + intent.getAction());

        switch (intent.getAction()) {
            case ACTION_SAVE_OPEN_SCREEN:
                long time = intent.getLongExtra(TAG_TIME, 0);
                new RecordOpenScreen(time).save();
                break;

            case ACTION_CLEANING:
                new Delete().from(RecordOpenScreen.class).execute();
                break;
        }
    }
}
