package com.dml.test.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dml.test.constans.AppTheme;

/**
 * Created by Dzmitry Lukashanets on 31.03.2015.
 */
public class ThemeAdapter extends BaseAdapter {

    private AppTheme[] mThemes = AppTheme.values();
    private Context mContext;

    public ThemeAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mThemes.length;
    }

    public AppTheme getItemTheme(int position) {
        return (AppTheme) getItem(position);
    }

    @Override
    public Object getItem(int position) {
        return mThemes[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(android.R.layout.simple_list_item_single_choice, parent, false);
            vh = new ViewHolder();
            vh.tv = (TextView) convertView.findViewById(android.R.id.text1);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        AppTheme item = mThemes[position];
        vh.tv.setText(item.getIdName());
        convertView.setBackgroundResource(item.getIdColor());
        return convertView;
    }

    class ViewHolder {
        TextView tv;
    }
}
