package com.dml.test.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.dml.test.entity.RecordOpenScreen;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Dzmitry Lukashanets on 31.03.2015.
 */
public class ListRecordsAdapter extends CursorAdapter {

    private SimpleDateFormat mSdf = new SimpleDateFormat("dd MMM yyyy   HH:mm:ss", Locale.getDefault());

    public ListRecordsAdapter(Context context, Cursor c) {
        super(context, c, false);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1, parent, false);
        ViewHolder vh = new ViewHolder();
        vh.tv1 = (TextView) view.findViewById(android.R.id.text1);
        view.setTag(vh);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder vh = (ViewHolder) view.getTag();
        vh.tv1.setText(mSdf.format(new Date(cursor.getLong(cursor.getColumnIndex(RecordOpenScreen.TIME)))));
    }

    protected class ViewHolder {
        TextView tv1;
    }
}
