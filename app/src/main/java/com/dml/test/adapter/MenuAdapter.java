package com.dml.test.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dml.test.constans.ItemMenu;

/**
 * Created by Dzmitry Lukashanets on 31.03.2015.
 */
public class MenuAdapter extends BaseAdapter {

    private ItemMenu[] mItems = ItemMenu.values();
    private Context mContext;

    public MenuAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mItems.length;
    }

    public ItemMenu getItemMenu(int position) {
        return (ItemMenu) getItem(position);
    }

    @Override
    public Object getItem(int position) {
        return mItems[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(android.R.layout.simple_list_item_1, parent, false);
            vh = new ViewHolder();
            vh.tv = (TextView) convertView.findViewById(android.R.id.text1);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        ItemMenu item = mItems[position];
        vh.tv.setText(item.getNameId());
        return convertView;
    }

    class ViewHolder {
        TextView tv;
    }
}
