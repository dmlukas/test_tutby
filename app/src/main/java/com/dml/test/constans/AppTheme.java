package com.dml.test.constans;

import com.dml.test.R;

/**
 * Created by Dzmitry Lukashanets on 01.04.2015.
 */
public enum AppTheme {
    APP_THEME_DEFAULT(R.style.AppTheme, R.color.bg_default, R.string.theme_default),
    APP_THEME_1(R.style.AppTheme1, R.color.bg_1, R.string.theme_1),
    APP_THEME_2(R.style.AppTheme2, R.color.bg_2, R.string.theme_2);

    private AppTheme(int idTheme, int idColor, int idName) {
        this.mIdTheme = idTheme;
        this.mIdColor = idColor;
        this.mIdName = idName;
    }

    private int mIdTheme;
    private int mIdColor;
    private int mIdName;

    public int getIdTheme() {
        return mIdTheme;
    }

    public int getIdColor() {
        return mIdColor;
    }

    public int getIdName() {
        return mIdName;
    }
}
