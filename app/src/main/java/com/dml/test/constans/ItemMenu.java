package com.dml.test.constans;

import com.dml.test.R;

/**
 * Created by Dzmitry Lukashanets on 31.03.2015.
 */
public enum ItemMenu {

    MAIN(R.string.menu_main), SETTINGS(R.string.menu_settings);

    private ItemMenu(int nameID) {
        this.mNameId = nameID;
    }

    private int mNameId;

    public int getNameId() {
        return mNameId;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
