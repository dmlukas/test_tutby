package com.dml.test.ui.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.activeandroid.Cache;
import com.activeandroid.content.ContentProvider;
import com.dml.test.R;
import com.dml.test.entity.RecordOpenScreen;
import com.dml.test.service.AppIntentService;
import com.dml.test.ui.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Dzmitry Lukashanets on 31.03.2015.
 */
public class ScreenFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String TAG_FG = "ScreenFragment";
    private static final String TAG_ID = "id";
    private static final String TAG_TIME = "time";
    private long mId = -1;
    private TextView tvTime;
    private long mTime;
    private ExecutorService mExecutor = Executors.newSingleThreadExecutor();
    private SimpleDateFormat mSdf = new SimpleDateFormat("dd MMM yyyy   HH:mm:ss", Locale.getDefault());

    public static ScreenFragment newInstance(FragmentManager fgm) {
        return newInstance(fgm, -1);
    }

    public static ScreenFragment newInstance(FragmentManager fgm, long id) {
        Fragment fg = fgm.findFragmentByTag(TAG_FG);
        if (fg == null) {
            fg = new ScreenFragment();
            Bundle bundle = new Bundle();
            bundle.putLong(TAG_ID, id);
            fg.setArguments(bundle);
        }
        return (ScreenFragment) fg;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mId = bundle.getLong(TAG_ID);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setTitleActionBar(R.string.screen);

        if (mId == -1) {
            if (savedInstanceState == null) {
                mTime = System.currentTimeMillis();
                AppIntentService.saveOpenScreen(getActivity(), mTime);
            } else {
                mTime = savedInstanceState.getLong(TAG_TIME);
            }
            setTextTime(mTime);
        } else {
            Log.i(TAG_FG, "id = " + mId);
            getLoaderManager().restartLoader(MainActivity.LOADER_SCREEN, null, this);
        }
    }

    @Override
    public void onDestroyView() {
        getLoaderManager().destroyLoader(MainActivity.LOADER_SCREEN);
        super.onDestroyView();
    }

    private void setTextTime(long time) {
        tvTime.setText(mSdf.format(new Date(time)));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mId == -1) {
            outState.putLong(TAG_TIME, mTime);
        }
    }

    @Override
    public String getTagFg() {
        return TAG_FG;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_screen;
    }

    @Override
    protected void initUI(View view) {
        tvTime = (TextView) view.findViewById(R.id.tv_time);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(),
                ContentProvider.createUri(RecordOpenScreen.class, null),
                new String[]{RecordOpenScreen.TIME},
                Cache.getTableInfo(RecordOpenScreen.class).getIdName() + "=?",
                new String[]{String.valueOf(mId)},
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.i(TAG_FG, "" + data.getCount());
        if (data.moveToFirst()) {
            setTextTime(data.getLong(data.getColumnIndex(RecordOpenScreen.TIME)));
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

}
