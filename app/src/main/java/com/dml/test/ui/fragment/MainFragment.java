package com.dml.test.ui.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.content.ContentProvider;
import com.dml.test.R;
import com.dml.test.adapter.ListRecordsAdapter;
import com.dml.test.entity.RecordOpenScreen;
import com.dml.test.ui.MainActivity;

/**
 * Created by Dzmitry Lukashanets on 31.03.2015.
 */
public class MainFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String TAG_FG = "MainFragment";

    private ListView lvList;
    private TextView tvEmpty, tvLastCleaning, tvNextCleaning;
    private ListRecordsAdapter mAdapter;
    private final Handler handler = new Handler();

    public static MainFragment newInstance(FragmentManager fgm) {
        Fragment fg = fgm.findFragmentByTag(TAG_FG);
        if (fg == null) {
            fg = new MainFragment();
        }
        return (MainFragment) fg;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new ListRecordsAdapter(getActivity(), null);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setTitleActionBar(R.string.main);
        lvList.setAdapter(mAdapter);
        getLoaderManager().restartLoader(MainActivity.LOADER_LIST, null, this);
        handler.post(runSetText);
    }

    private Runnable runSetText = new Runnable() {
        @Override
        public void run() {
            setLastTimeCleanValue(getMainActivity().getLastTimeCleanValue());
            setNextTimeCleaning(getMainActivity().getTimeOutCleanValue());
            handler.postDelayed(runSetText, 1000);
        }
    };


    public void setLastTimeCleanValue(String str) {
        if (str == null) {
            tvLastCleaning.setText("Last cleaning: ");
        } else {
            tvLastCleaning.setText("Last cleaning: " + str);
        }
    }

    public void setNextTimeCleaning(String str) {
        if (str == null) {
            tvNextCleaning.setText("Next cleaning: ");
        } else {
            tvNextCleaning.setText("Next cleaning: " + str);
        }
    }

    @Override
    public void onDestroyView() {
        handler.removeCallbacksAndMessages(null);
        getLoaderManager().destroyLoader(MainActivity.LOADER_LIST);
        super.onDestroyView();
    }

    @Override
    public String getTagFg() {
        return TAG_FG;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_main;
    }

    @Override
    protected void initUI(View view) {
        view.findViewById(R.id.btn_start_screen).setOnClickListener(this);
        lvList = (ListView) view.findViewById(R.id.lv_list);
        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getMainActivity().showFragmentOnTop(ScreenFragment.newInstance(getFragmentManager(), id));
            }
        });
        tvEmpty = (TextView) view.findViewById(R.id.tv_empty);
        tvLastCleaning = (TextView) view.findViewById(R.id.tv_time_last_cleaning);
        tvNextCleaning = (TextView) view.findViewById(R.id.tv_time_next_cleaning);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_start_screen:
                getMainActivity().showFragmentOnTop(ScreenFragment.newInstance(getFragmentManager()));
                break;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), ContentProvider.createUri(RecordOpenScreen.class, null), null, null, null, RecordOpenScreen.TIME + " DESC");
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data.getCount() == 0) {
            tvEmpty.setVisibility(View.VISIBLE);
        } else {
            tvEmpty.setVisibility(View.GONE);
        }
        mAdapter.changeCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
    }
}
