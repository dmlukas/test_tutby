package com.dml.test.ui.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.dml.test.R;
import com.dml.test.utils.PrefHelper;

/**
 * Created by Dzmitry Lukashanets on 31.03.2015.
 */
public class SettingsFragment extends BaseFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String TAG_FG = "SettingsFragment";

    private TextView tvTimeOut;

    public static SettingsFragment newInstance(FragmentManager fgm) {
        Fragment fg = fgm.findFragmentByTag(TAG_FG);
        if (fg == null) {
            fg = new SettingsFragment();
        }
        return (SettingsFragment) fg;
    }

    @Override
    public String getTagFg() {
        return TAG_FG;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_settings;
    }

    @Override
    protected void initUI(View view) {
        tvTimeOut = (TextView) view.findViewById(R.id.tv_timeout);
        view.findViewById(R.id.ll_timeout).setOnClickListener(this);
        view.findViewById(R.id.ll_theme).setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setTitleActionBar(R.string.settings);
        setTextTimeOut(PrefHelper.getTimeOutClean(getActivity()));
        PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onDestroyView() {
        PreferenceManager.getDefaultSharedPreferences(getActivity()).unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroyView();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case PrefHelper.TIME_OUT_CLEAN:
                setTextTimeOut(sharedPreferences.getInt(key, PrefHelper.DEFAULT_TIME_OUT_CLEAN));
                break;
        }
    }

    private void setTextTimeOut(int timeOut) {
        tvTimeOut.setText(String.valueOf(timeOut) + " " + getString(R.string.min));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ll_timeout:
                TimeOutDialog dialog = TimeOutDialog.newInstance(getChildFragmentManager());
                dialog.show(getChildFragmentManager(), TimeOutDialog.TAG_FG);
                break;

            case R.id.ll_theme:
                getMainActivity().showFragmentOnTop(ThemeFragment.newInstance(getFragmentManager()));
                break;
        }
    }


    public static class TimeOutDialog extends DialogFragment {

        public static final String TAG_FG = "TimeOutDialog";

        public static TimeOutDialog newInstance(FragmentManager fgm) {
            Fragment fg = fgm.findFragmentByTag(TAG_FG);
            if (fg == null) {
                fg = new TimeOutDialog();
            }
            return (TimeOutDialog) fg;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.view_dialog_timeout, null);
            final EditText edtTimeOut = (EditText) view.findViewById(R.id.tv_timeout);
            edtTimeOut.setText(String.valueOf(PrefHelper.getTimeOutClean(getActivity())));
            edtTimeOut.setSelection(edtTimeOut.getText().length());
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setView(view)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (!TextUtils.isEmpty(edtTimeOut.getText())) {
                                PrefHelper.setTimeOutClean(getActivity(), Integer.parseInt(edtTimeOut.getText().toString()));
                            }
                        }
                    })
                    .setNegativeButton(R.string.cancel, null);

            return builder.create();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            return super.onCreateView(inflater, container, savedInstanceState);
        }
    }
}
