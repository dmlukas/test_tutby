package com.dml.test.ui.fragment;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.dml.test.R;
import com.dml.test.adapter.ThemeAdapter;
import com.dml.test.constans.AppTheme;
import com.dml.test.utils.PrefHelper;

/**
 * Created by Dzmitry Lukashanets on 01.04.2015.
 */
public class ThemeFragment extends BaseFragment {

    public static final String TAG_FG = "ThemeFragment";

    private ThemeAdapter mAdapter;
    private AppTheme mCurrentTheme;

    public static ThemeFragment newInstance(FragmentManager fgm) {
        Fragment fg = fgm.findFragmentByTag(TAG_FG);
        if (fg == null) {
            fg = new ThemeFragment();
        }
        return (ThemeFragment) fg;
    }

    @Override
    public String getTagFg() {
        return TAG_FG;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_theme;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCurrentTheme = PrefHelper.getTheme(getActivity());
        mAdapter = new ThemeAdapter(getActivity());
    }

    @Override
    protected void initUI(View view) {
        view.findViewById(R.id.btn_cancel).setOnClickListener(this);
        view.findViewById(R.id.btn_ok).setOnClickListener(this);
        ListView listView = (ListView) view.findViewById(R.id.lv_list);
        listView.setAdapter(mAdapter);
        listView.setItemChecked(mCurrentTheme.ordinal(), true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mCurrentTheme = mAdapter.getItemTheme(position);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setTitleActionBar(R.string.theme);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_cancel:
                getMainActivity().onBackPressed();
                break;

            case R.id.btn_ok:
                if (PrefHelper.getTheme(getActivity()) != mCurrentTheme) {
                    PrefHelper.setTheme(getActivity(), mCurrentTheme);
                    getActivity().setTheme(mCurrentTheme.getIdTheme());
                    getMainActivity().recreate();
                } else {
                    getMainActivity().onBackPressed();
                }
                break;
        }
    }
}
