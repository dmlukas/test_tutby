package com.dml.test.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dml.test.ui.MainActivity;

/**
 * Created by Dzmitry Lukashanets on 31.03.2015.
 */
public abstract class BaseFragment extends Fragment implements View.OnClickListener {


    public abstract String getTagFg();

    protected abstract int getLayoutID();

    protected abstract void initUI(View view);

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    protected MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(getLayoutID(), container, false);
        initUI(v);
        return v;
    }

    protected void setTitleActionBar(int id) {
        getMainActivity().getSupportActionBar().setTitle(id);
    }

    @Override
    public void onClick(View v) {

    }
}
