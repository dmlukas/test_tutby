package com.dml.test.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.dml.test.AppApplication;
import com.dml.test.R;
import com.dml.test.constans.AppTheme;
import com.dml.test.constans.ItemMenu;
import com.dml.test.service.AppIntentService;
import com.dml.test.ui.fragment.BaseFragment;
import com.dml.test.ui.fragment.MainFragment;
import com.dml.test.ui.fragment.NavigationDrawerFragment;
import com.dml.test.ui.fragment.SettingsFragment;
import com.dml.test.utils.PrefHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, FragmentManager.OnBackStackChangedListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private class DownTimer extends CountDownTimer {

        private static final String TAG = "DownTimer";

        private SimpleDateFormat mSdfLast = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public DownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        public String convert(long millis) {
            int sec = (int) (millis / 1000) % 60;
            int min = (int) ((millis / (1000 * 60)) % 60);
            int hr = (int) ((millis / (1000 * 60 * 60)) % 24);
            return String.format("%d:%02d:%02d", hr, min, sec);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            mTickTime = millisUntilFinished;
            mTimeOutCleanValue = convert(millisUntilFinished);
            Log.i(TAG, mTimeOutCleanValue);
        }

        @Override
        public void onFinish() {
            AppIntentService.startCleaning(MainActivity.this);
            mLastTimeCleanValue = mSdfLast.format(new Date(System.currentTimeMillis()));
            Log.i(TAG, mLastTimeCleanValue);
            mTimer = (DownTimer) new DownTimer(PrefHelper.getTimeOutCleanInMillis(MainActivity.this), 1000).start();
        }
    }

    public static final String TAG_RECREATE = "recreate";
    private static final String TAG_LAST_TIME_CLEAN = "lastTimeClean";
    private static final String TAG_TICK_TIME = "tickTime";
    private static final String TAG_DESTROY = "destroy";

    public static final int LOADER_SCREEN = 1;
    public static final int LOADER_LIST = 2;

    private NavigationDrawerFragment mNavigationDrawerFragment;

    private String mLastTimeCleanValue;
    private String mTimeOutCleanValue;
    private DownTimer mTimer;
    private long mTickTime;

    @Override
    public void recreate() {
        if (AppApplication.hasHoneycomb()) {
            onBackPressed();
            super.recreate();
        } else {
            Bundle temp_bundle = new Bundle();
            onSaveInstanceState(temp_bundle);
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(TAG_RECREATE, temp_bundle);
            startActivity(intent);
            finish();
        }
    }

    public String getTimeOutCleanValue() {
        return mTimeOutCleanValue;
    }

    public String getLastTimeCleanValue() {
        return mLastTimeCleanValue;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppTheme theme = PrefHelper.getTheme(this);
        if (theme != AppTheme.APP_THEME_DEFAULT) {
            setTheme(theme.getIdTheme());
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        onBackStackChanged();

        long startTime = 0;

        if (savedInstanceState == null) {
            startTime = PrefHelper.getTimeOutCleanInMillis(this);
            if (getIntent() != null) {
                Bundle bundle = getIntent().getBundleExtra(TAG_RECREATE);
                if (bundle != null) {
                    mLastTimeCleanValue = bundle.getString(TAG_LAST_TIME_CLEAN);
                    startTime = considerDestroyActivity(bundle);
                    showFragmentDrawer(SettingsFragment.newInstance(getSupportFragmentManager()));
                }
            }
        } else {
            mLastTimeCleanValue = savedInstanceState.getString(TAG_LAST_TIME_CLEAN);
            startTime = considerDestroyActivity(savedInstanceState);
        }
        mTimer = (DownTimer) new DownTimer(startTime, 1000).start();
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
    }

    private long considerDestroyActivity(Bundle bundle) {
        return bundle.getLong(TAG_TICK_TIME) - System.currentTimeMillis() + bundle.getLong(TAG_DESTROY);
    }

    @Override
    protected void onDestroy() {
        mTimer.cancel();
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
        getSupportFragmentManager().removeOnBackStackChangedListener(this);
        super.onDestroy();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case PrefHelper.TIME_OUT_CLEAN:
                mTimer.cancel();
                mTimer = (DownTimer) new DownTimer(sharedPreferences.getInt(key, PrefHelper.DEFAULT_TIME_OUT_CLEAN) * 60 * 1000, 1000).start();
                break;
        }
    }

    private int getBackStackEntryCount() {
        return getSupportFragmentManager().getBackStackEntryCount();
    }

    @Override
    public void onNavigationDrawerItemSelected(ItemMenu item) {
        BaseFragment fg;

        switch (item) {
            case MAIN:
                fg = MainFragment.newInstance(getSupportFragmentManager());
                break;

            case SETTINGS:
                fg = SettingsFragment.newInstance(getSupportFragmentManager());
                break;

            default:
                fg = MainFragment.newInstance(getSupportFragmentManager());
                break;
        }
        showFragmentDrawer(fg);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TAG_LAST_TIME_CLEAN, mLastTimeCleanValue);
        outState.putLong(TAG_TICK_TIME, mTickTime);
        outState.putLong(TAG_DESTROY, System.currentTimeMillis());
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen()) {
            mNavigationDrawerFragment.closeDrawer();
            return;
        }
        super.onBackPressed();
    }

    public void showFragmentDrawer(@NonNull BaseFragment fragment) {
        showFragment(fragment, true, false);
    }

    public void showFragmentOnTop(@NonNull BaseFragment fragment) {
        showFragment(fragment, false, true);
    }

    public void showFragment(@NonNull BaseFragment fragment, boolean clearStack, boolean addInBackStack) {
        if (fragment.isResumed()) {
            return;
        }
        if (clearStack) {
            while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStackImmediate();
            }
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, fragment.getTagFg());

        if (addInBackStack) {
            transaction.addToBackStack(fragment.getClass().getSimpleName());
        }
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commit();
    }

    @Override
    public void onBackStackChanged() {
        if (getBackStackEntryCount() == 0) {
            mNavigationDrawerFragment.setDrawerIndicatorEnabled(true);
        } else {
            mNavigationDrawerFragment.setDrawerIndicatorEnabled(false);
        }
    }


}
