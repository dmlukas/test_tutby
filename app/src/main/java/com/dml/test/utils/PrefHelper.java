package com.dml.test.utils;

import android.content.Context;
import android.preference.PreferenceManager;

import com.dml.test.constans.AppTheme;

/**
 * Created by Dzmitry Lukashanets on 31.03.2015.
 */
public class PrefHelper {

    public static final String TIME_OUT_CLEAN = "timeOutClean";
    public static final int DEFAULT_TIME_OUT_CLEAN = 5;

    public static final String THEME = "theme";

    public static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

    private PrefHelper() {
    }

    public static void setTimeOutClean(Context context, int min) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(TIME_OUT_CLEAN, min).commit();
    }

    public static int getTimeOutClean(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(TIME_OUT_CLEAN, DEFAULT_TIME_OUT_CLEAN);
    }

    public static int getTimeOutCleanInMillis(Context context) {
        return getTimeOutClean(context) * 60 * 1000;
    }

    public static void setTheme(Context context, AppTheme appTheme) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(THEME, appTheme.ordinal()).commit();
    }

    public static AppTheme getTheme(Context context) {
        int ordinal = PreferenceManager.getDefaultSharedPreferences(context).getInt(THEME, AppTheme.APP_THEME_DEFAULT.ordinal());
        return AppTheme.values()[ordinal];
    }

    public static void setUserLearnedDrawer(Context context, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(PREF_USER_LEARNED_DRAWER, value).commit();
    }

    public static boolean getUserLearnedDrawer(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PREF_USER_LEARNED_DRAWER, false);
    }


}
