package com.dml.test.entity;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by Dzmitry Lukashanets on 31.03.2015.
 */
@Table(name = "RecordsOpenScreen", id = BaseColumns._ID)
public class RecordOpenScreen extends Model {

    public RecordOpenScreen() {
        super();
    }

    public RecordOpenScreen(long time) {
        super();
        this.time = time;
    }

    public static final String TIME = "time";
    @Column(name = TIME)
    private long time;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
